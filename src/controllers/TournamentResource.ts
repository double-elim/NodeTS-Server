import { Request, Response } from 'express';
import { TournamentService } from '../service/TournamentService';

const tournamentService = new TournamentService();
export class TournamentResource {
  createTournament(req: Request, res: Response): void {
    const tournamentName: string = req.body.tournamentName;

    tournamentService.createTournament(tournamentName);
  }
}
